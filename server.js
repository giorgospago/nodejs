require('dotenv').config();

var express         = require("express");
var cors            = require('cors');
var nodemailer      = require('nodemailer');
var socketio        = require('socket.io');
var mongoose        = require('mongoose');

var app = express();

// MongoDB Connection
// mongoose.connect('mongodb://localhost/test', { useMongoClient: true });
mongoose.connect('mongodb://superuser:superpassword@develobird.gr:37017/1824test?authSource=admin', { useMongoClient: true });
mongoose.Promise = global.Promise;


var User = mongoose.model('User', {
    name: String,
    surname: String,
    phone: String,
    age: Number,
    email: String
});


global.io = socketio(app.listen(5050));
io.on('connection', function(socket){
    console.log(socket.id, ' connected !');

    socket.on('disconnect', function(){
        console.log(socket.id,' disconnected');
        console.log('-----------------------------------');
    });


    socket.on('new_message', function(data){
        data.id = socket.id;
        io.emit('global_messages', data);

    });
});




var userRouting = require('./routes/user');


var Mail = nodemailer.createTransport({
    host: process.env.SMTP_HOST,
    port: process.env.SMTP_PORT
});


app.use(cors());
app.use(express.static('files'));

app.get("/", function(req,res){
    res.sendFile(__dirname+"/files/home.html");
});

app.get("/about", function(req, res){

    var karidakis = new User({
        name: "Stelios2",
        surname: "Karidakis",
        phone: '6988579929',
        age: 31,
        email: "kar@gmail.com"
    });
    karidakis.save();

    res.send('ola ok');
});

app.get("/products", function(req, res){
    User.find(function(err, users){
        res.json(users);
    });
});

app.get("/mail", function(req,res){

    Mail.sendMail({
        from: "info@charmstyle.gr",
        to: "karydakis@gmail.com",
        subject: "Hhdghdg ",
        text: "fdgshdtbg dgdfg dfg dfgsdfgsfdh dfgdfgdf fg"
    });

    res.send("ta email stalthikan me epityxeia");

});

app.use("/user", userRouting);
