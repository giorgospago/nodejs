var mysql = require('mysql');
require('dotenv').config();


var myObject = {};

myObject.karydakhsName1 = function(){
    console.log("Stelios 1");
};

myObject.karydakhsName2 = function(){
    console.log("Stelios 2");
};

myObject.karydakhsName3 = function(){
    console.log("Stelios 3");
};

myObject.getProducts = function(){
    var con = mysql.createConnection({
        host     : process.env.DB_HOST,
        user     : process.env.DB_USER,
        password : process.env.DB_PASS,
        database : process.env.DB_NAME
    });
    con.connect();
    
    var products = [];
    con.query("SELECT * FROM products", function(err, result){
        con.end();
        return result;
    });
};

module.exports = myObject;